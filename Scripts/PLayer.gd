extends Area2D
signal updateScore
signal giveScore(score)
var Global = null

onready var world = get_tree().get_current_scene()

const SPEED = 5
const GRAVITY = 3
const MAX_FALL_SPEED = 8
const JUMP_DECREASE = 2
const MIN_JUMP_SPEED = 5
const JUMP_SPEED = -20

var yMovement = GRAVITY
var jumping
var score = 0

func _ready():
	Global = get_node("/root/Global")
	position.y = get_viewport().size.y / 2
	
	
func _physics_process(delta):
	position.x += SPEED
	
	if Input.is_action_just_pressed("ui_accept"):
		jumping = true
		yMovement = JUMP_SPEED
		$AnimatedSprite.play("up")
		$Wing.play()
	
	handle_jump()	
	
	position.y += yMovement
	
	if position.y > get_viewport().size.y:
		lost()


func handle_jump():
	if jumping:
		$AnimatedSprite.play("midFlap")
		yMovement = move_toward(yMovement, MIN_JUMP_SPEED, JUMP_DECREASE)
		if yMovement == MIN_JUMP_SPEED:
			jumping = false
	else:
		$AnimatedSprite.play("down")
		yMovement = move_toward(yMovement, MAX_FALL_SPEED, GRAVITY)


func _on_Player_body_entered(body):
	lost()
	
func lost():
	Global.playDiedMusic()
	Global.addScore(score)
	get_tree().change_scene("Scenes/Menu.tscn")


func add_score():
	score += 1
	world.update_score()
